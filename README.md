# README #

### Para que serve este repositório? ###

* Este repositório tem como finalidade armazenar o código fonte do trabalho de pós graduação da disciplina Arquitetura e Implementação OO do professor Denis Gonçalves Cople.
* 1.0

### Como eu configuro o projeto? ###

* Abra a solution no Visual Studio 2019
* No terminal, na raiz do projeto, execute o comando dotnet ef database update para que o banco de dados seja criado
* Aperte F5 ou clique no botão play do Debugger
* Database configuration

### Com quem falar? ###

* Fabio Romariz - romariz @ gmail