﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SeguroSaude.Command.Interface;
using SeguroSaude.Models;
using System;

namespace SeguroSaude.Command
{
    public class ContratoCommand : Controller, ICommand
    {
        public IActionResult DoDelete(HttpRequest request)
        {
            throw new NotImplementedException();
        }

        public IActionResult DoGet(HttpRequest request)
        {
            throw new NotImplementedException();
        }

        public IActionResult DoPost(HttpRequest request)
        {
            throw new NotImplementedException();
        }

        public IActionResult DoPut(HttpRequest request)
        {
            throw new NotImplementedException();
        }

        public IActionResult Execute(HttpRequest request, HttpResponse response)
        {
            return View("~/Views/Contrato/Index.cshtml");
        }
    }
}
