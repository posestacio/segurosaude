﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace SeguroSaude.Command.Interface
{
    public interface ICommand
    {
        IActionResult Execute(HttpRequest request, HttpResponse response);
        IActionResult DoGet(HttpRequest request);
        IActionResult DoPost(HttpRequest request);
        IActionResult DoPut(HttpRequest request);
        IActionResult DoDelete(HttpRequest request);
    }
}
