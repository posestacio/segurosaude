﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SeguroSaude.Command.Interface;
using SeguroSaude.Entity;
using SeguroSaude.Models;
using SeguroSaude.Models.Helpers;
using SeguroSaude.Repository.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SeguroSaude.Command
{
    public class OperadoraCommand : Controller, ICommand
    {
        private readonly IRepositoryBase<Operadora> _repository;
        IMapper _mapper;
        private OperadoraModel Model { get; set; }
        public OperadoraCommand(IRepositoryBase<Operadora> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public IActionResult Execute(HttpRequest request, HttpResponse response)
        {
            if (request.HasFormContentType)
            {
                Model = ModelHelper<OperadoraModel>.GetFormData(request.Form);
            }

            if (request.Method == "GET")
            {
                return DoGet(request);
            }
            if (request.Method == "POST" && !request.Query.Any(x => x.Value == "RemoveOperadora"))
            {
                if (Model.Id == Guid.Empty)
                {
                    return DoPost(request);
                }
                else
                {
                    return DoPut(request);
                }
            }
            if (request.Method == "POST" && request.Query.Any(x => x.Value == "RemoveOperadora"))
            {
                return DoDelete(request);
            }

            return BadRequest("Não é possível executar a ação.");
        }

        public IActionResult DoGet(HttpRequest request)
        {
            try
            {
                if (request.Query.Any(x => x.Value == "Operadoras"))
                {
                    var operadoras = _repository.ReadAll().ToList();
                    return View("~/Views/Operadora/Index.cshtml", _mapper.Map<List<OperadoraModel>>(operadoras));
                }
                else if (request.Query.Any(x => x.Value == "RemoveOperadora"))
                {
                    var operadora = _repository.Read(new Guid(request.Query.FirstOrDefault(x => x.Key == "guid").Value));
                    return View("~/Views/Operadora/Delete.cshtml", _mapper.Map<OperadoraModel>(operadora));
                }
                else
                {
                    if (request.Query.Any(x => x.Key == "guid"))
                    {
                        var operadora = _repository.Read(new Guid(request.Query.FirstOrDefault(x => x.Key == "guid").Value));
                        return View("~/Views/Operadora/Edit.cshtml", _mapper.Map<OperadoraModel>(operadora));
                    }
                    else
                    {
                        return View("~/Views/Operadora/Edit.cshtml", new OperadoraModel());
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
        public IActionResult DoPost(HttpRequest request)
        {
            try
            {
                Model.Id = Guid.NewGuid();
                _repository.Create(_mapper.Map<Operadora>(Model));

                var operadoras = _repository.ReadAll().ToList();
                return View("~/Views/Operadora/Index.cshtml", _mapper.Map<List<OperadoraModel>>(operadoras));
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        public IActionResult DoPut(HttpRequest request)
        {
            try
            {
                _repository.Update(_mapper.Map<Operadora>(Model));

                var operadoras = _repository.ReadAll().ToList();
                return View("~/Views/Operadora/Index.cshtml", _mapper.Map<List<OperadoraModel>>(operadoras));
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
        public IActionResult DoDelete(HttpRequest request)
        {
            _repository.Delete(Model.Id);

            var operadoras = _repository.ReadAll().ToList();
            return View("~/Views/Operadora/Index.cshtml", _mapper.Map<List<OperadoraModel>>(operadoras));
        }
    }
}
