﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SeguroSaude.Command.Interface;
using SeguroSaude.Models;

namespace SeguroSaude.Command
{
    public class CorretoraCommand : Controller, ICommand
    {
        public IActionResult DoDelete(HttpRequest request)
        {
            throw new System.NotImplementedException();
        }

        public IActionResult DoGet(HttpRequest request)
        {
            throw new System.NotImplementedException();
        }

        public IActionResult DoPost(HttpRequest request)
        {
            throw new System.NotImplementedException();
        }

        public IActionResult DoPut(HttpRequest request)
        {
            throw new System.NotImplementedException();
        }

        public IActionResult Execute(HttpRequest request, HttpResponse response)
        {
            return View("~/Views/Corretora/Index.cshtml");
        }
    }
}
