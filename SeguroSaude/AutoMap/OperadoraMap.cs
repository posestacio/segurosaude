﻿using AutoMapper;
using SeguroSaude.Entity;
using SeguroSaude.Models;

namespace SeguroSaude.AutoMap
{
    public class OperadoraMap : Profile
    {
        public OperadoraMap()
        {
            CreateMap<Operadora, OperadoraModel>();
            CreateMap<OperadoraModel, Operadora>();
        }
    }
}
