﻿using SeguroSaude.Entity.Base;

namespace SeguroSaude.Entity
{
    public class Pessoa : EntityBase
    {
        public string Nome { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
        public string Endereco { get; set; }

        public override bool IsValid()
        {
            return (!string.IsNullOrEmpty(Nome) && !string.IsNullOrEmpty(Telefone) && !string.IsNullOrEmpty(Email) && !string.IsNullOrEmpty(Endereco));
        }
    }
}
