﻿using System;

namespace SeguroSaude.Entity.Base
{
    public abstract class EntityBase
    {
        public Guid Id { get; set; }
        public bool Ativo { get; set; }
        public abstract bool IsValid();
    }
}
