﻿using System;

namespace SeguroSaude.Entity
{
    public class PessoaFisica : Pessoa
    {
        public string CPF { get; set; }
        public DateTime DataNascimento { get; set; }
    }
}
