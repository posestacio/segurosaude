﻿using System;

namespace SeguroSaude.Entity
{
    public class Corretor : PessoaFisica
    {
        public Guid? CorretoraId { get; set; }
        public virtual Corretora Corretora { get; set; }
        public string Matricula { get; set; }
    }
}
