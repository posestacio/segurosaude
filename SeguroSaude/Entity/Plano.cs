﻿using SeguroSaude.Entity.Base;
using System;

namespace SeguroSaude.Entity
{
    public class Plano : EntityBase
    {
        public Guid OperadoraId { get; set; }
        public virtual Operadora Operadora { get; set; }
        public string Nome { get; set; }
        public decimal Valor { get; set; }

        public override bool IsValid()
        {
            return OperadoraId != Guid.Empty &&
                !string.IsNullOrEmpty(Nome) &&
                Valor > 0;
        }
    }
}
