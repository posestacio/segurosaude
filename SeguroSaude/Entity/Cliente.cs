﻿using System;

namespace SeguroSaude.Entity
{
    public class Cliente : PessoaFisica
    {
        public Cliente()
        {
            ClienteDesde = DateTime.Now;
        }
        public DateTime ClienteDesde { get; set; }
    }
}
