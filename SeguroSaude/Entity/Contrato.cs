﻿using SeguroSaude.Entity.Base;
using System;

namespace SeguroSaude.Entity
{
    public class Contrato : EntityBase
    {
        public DateTime DataInicio { get; set; }
        public int Numero { get; set; }
        public Guid ClienteId { get; set; }
        public virtual Cliente Cliente { get; set; }
        public Guid PlanoId { get; set; }
        public virtual Plano Plano { get; set; }
        public Guid? CorretorId { get; set; }
        public virtual Corretor Corretor { get; set; }
        public Guid? CorretoraId { get; set; }
        public virtual Corretora Corretora { get; set; }
        public override bool IsValid()
        {
            return DataInicio >= DateTime.Now && 
                Numero > 0 && 
                ((CorretoraId.HasValue && CorretoraId.Value != Guid.Empty) || (CorretorId.HasValue && CorretorId.Value != Guid.Empty)) && 
                ClienteId != Guid.Empty && 
                PlanoId != Guid.Empty;
        }
    }
}
