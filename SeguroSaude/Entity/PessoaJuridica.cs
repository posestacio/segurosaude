﻿using System;

namespace SeguroSaude.Entity
{
    public class PessoaJuridica : Pessoa
    {
        public string CNPJ { get; set; }
        public string RazaoSocial { get; set; }
    }
}
