﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SeguroSaude.Command;
using SeguroSaude.Command.Interface;
using SeguroSaude.Entity;
using SeguroSaude.Models;
using SeguroSaude.Repository.Base;
using System;
using System.Diagnostics;
using System.Linq;

namespace SeguroSaude.Controllers.FrontController
{
    public class SeguroSaudeController : Controller
    {
        private readonly string[] operadoraCommands = new string[] { "Operadora", "Operadoras", "RemoveOperadora" };

        private readonly ILogger<SeguroSaudeController> _logger;
        private readonly IRepositoryBase<Operadora> _operadoraRepository;
        IMapper _mapper;
        public SeguroSaudeController(
            ILogger<SeguroSaudeController> logger,
            IRepositoryBase<Operadora> operadoraRepository,
            IMapper mapper
        )
        {
            _logger = logger;
            _operadoraRepository = operadoraRepository;
            _mapper = mapper;
        }
        public IActionResult Index(string command)
        {
            var context = HttpContext;
            return ProcessCommand(command, context);
        }

        private IActionResult ProcessCommand(string command, HttpContext context, EntityBaseModel model = null)
        {
            var callerCommand = InstanciarComando(command);
            return callerCommand.Execute(context.Request, context.Response);
        }

        private ICommand InstanciarComando(string command)
        {
            if (operadoraCommands.Contains(command))
            {
                return new OperadoraCommand(_operadoraRepository, _mapper);
            }

            // TODO: Adicionar validação de outros comandos de outras funcionalidades aqui

            throw new NullReferenceException($"Não é possível instanciar classe Command para o comando {command}");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
