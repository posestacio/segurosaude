﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SeguroSaude.Models;
using System.Diagnostics;

namespace SeguroSaude.Controllers
{
    public class ContratoController : Controller
    {
        private readonly ILogger<ContratoController> _logger;
        public ContratoController(ILogger<ContratoController> logger)
        {
            _logger = logger;
        }
        public IActionResult Index()
        {
            return View();
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
