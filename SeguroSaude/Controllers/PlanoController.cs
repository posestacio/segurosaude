﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SeguroSaude.Models;
using System.Diagnostics;

namespace SeguroSaude.Controllers
{
    public class PlanoController : Controller
    {
        private readonly ILogger<PlanoController> _logger;
        public PlanoController(ILogger<PlanoController> logger)
        {
            _logger = logger;
        }
        public IActionResult Index()
        {
            return View();
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
