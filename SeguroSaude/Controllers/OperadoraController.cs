﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SeguroSaude.Models;
using System.Diagnostics;

namespace SeguroSaude.Controllers
{
    public class OperadoraController : Controller
    {
        private readonly ILogger<OperadoraController> _logger;
        public OperadoraController(ILogger<OperadoraController> logger)
        {
            _logger = logger;
        }
        public IActionResult Index()
        {
            return View();
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
