﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SeguroSaude.Data.Configuration;
using SeguroSaude.Entity;

namespace SeguroSaude.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        DbSet<Cliente> Cliente { get; set; }
        DbSet<Contrato> Contrato { get; set; }
        DbSet<Corretora> Corretora { get; set; }
        DbSet<Corretor> Corretor { get; set; }
        DbSet<Operadora> Operadora { get; set; }
        DbSet<Plano> Plano { get; set; }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            ClienteConfiguration.Configure(builder);
            ContratoConfiguration.Configure(builder);
            CorretoraConfiguration.Configure(builder);
            CorretorConfiguration.Configure(builder);
            OperadoraConfiguration.Configure(builder);
            PlanoConfiguration.Configure(builder);
        }
    }
}
