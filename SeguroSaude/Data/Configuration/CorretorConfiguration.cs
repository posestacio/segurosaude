﻿using Microsoft.EntityFrameworkCore;
using SeguroSaude.Entity;

namespace SeguroSaude.Data.Configuration
{
    public static class CorretorConfiguration
    {
        public static void Configure(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Corretor>()
                .HasKey(c => c.Id);
            modelBuilder.Entity<Corretor>()
                .Property(c => c.Ativo);
            modelBuilder.Entity<Corretor>()
                .Property(c => c.CPF)
                .HasMaxLength(11);
            modelBuilder.Entity<Corretor>()
                .Property(c => c.DataNascimento);
            modelBuilder.Entity<Corretor>()
                .Property(c => c.Email)
                .HasMaxLength(200);
            modelBuilder.Entity<Corretor>()
                .Property(c => c.Endereco)
                .HasMaxLength(400);
            modelBuilder.Entity<Corretor>()
                .Property(c => c.Matricula)
                .HasMaxLength(15);
            modelBuilder.Entity<Corretor>()
                .Property(c => c.Nome)
                .HasMaxLength(150);
            modelBuilder.Entity<Corretor>()
                .Property(c => c.Telefone)
                .HasMaxLength(11);
            modelBuilder.Entity<Corretor>()
                .HasOne(c => c.Corretora)
                .WithMany()
                .HasForeignKey(c => c.CorretoraId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
