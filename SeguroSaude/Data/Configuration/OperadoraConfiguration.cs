﻿using Microsoft.EntityFrameworkCore;
using SeguroSaude.Entity;

namespace SeguroSaude.Data.Configuration
{
    public static class OperadoraConfiguration
    {
        public static void Configure(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Operadora>()
                .HasKey(x => x.Id);
            modelBuilder.Entity<Operadora>()
                .Property(x => x.Ativo);
            modelBuilder.Entity<Operadora>()
                .Property(x => x.CNPJ)
                .HasMaxLength(15);
            modelBuilder.Entity<Operadora>()
                .Property(x => x.Email)
                .HasMaxLength(200);
            modelBuilder.Entity<Operadora>()
                .Property(x => x.Endereco)
                .HasMaxLength(400);
            modelBuilder.Entity<Operadora>()
                .Property(x => x.Logo);
            modelBuilder.Entity<Operadora>()
                .Property(x => x.Nome)
                .HasMaxLength(150);
            modelBuilder.Entity<Operadora>()
                .Property(x => x.RazaoSocial)
                .HasMaxLength(200);
            modelBuilder.Entity<Operadora>()
                .Property(x => x.Telefone)
                .HasMaxLength(11);
        }
    }
}
