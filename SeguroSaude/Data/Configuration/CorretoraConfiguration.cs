﻿using Microsoft.EntityFrameworkCore;
using SeguroSaude.Entity;

namespace SeguroSaude.Data.Configuration
{
    public static class CorretoraConfiguration
    {
        public static void Configure(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Corretora>()
                .HasKey(c => c.Id);
            modelBuilder.Entity<Corretora>()
                .Property(c => c.AptaAVender);
            modelBuilder.Entity<Corretora>()
                .Property(c => c.Ativo);
            modelBuilder.Entity<Corretora>()
                .Property(c => c.CNPJ)
                .HasMaxLength(15);
            modelBuilder.Entity<Corretora>()
                .Property(c => c.Email)
                .HasMaxLength(200);
            modelBuilder.Entity<Corretora>()
                .Property(c => c.Endereco)
                .HasMaxLength(400);
            modelBuilder.Entity<Corretora>()
                .Property(c => c.Nome)
                .HasMaxLength(150);
            modelBuilder.Entity<Corretora>()
                .Property(c => c.RazaoSocial)
                .HasMaxLength(200);
            modelBuilder.Entity<Corretora>()
                .Property(c => c.Telefone)
                .HasMaxLength(11);
        }
    }
}
