﻿using Microsoft.EntityFrameworkCore;
using SeguroSaude.Entity;

namespace SeguroSaude.Data.Configuration
{
    public static class PlanoConfiguration
    {
        public static void Configure(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Plano>()
                .HasKey(x => x.Id);
            modelBuilder.Entity<Plano>()
                .Property(x => x.Ativo);
            modelBuilder.Entity<Plano>()
                .Property(x => x.Nome)
                .HasMaxLength(100);
            modelBuilder.Entity<Plano>()
                .Property(x => x.Valor)
                .HasColumnType("decimal")
                .HasPrecision(18, 2);
            modelBuilder.Entity<Plano>()
                .HasOne(x => x.Operadora)
                .WithMany()
                .HasForeignKey(x => x.OperadoraId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
