﻿using Microsoft.EntityFrameworkCore;
using SeguroSaude.Entity;

namespace SeguroSaude.Data.Configuration
{
    public static class ContratoConfiguration
    {
        public static void Configure(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Contrato>()
                .HasKey(c => c.Id);
            modelBuilder.Entity<Contrato>()
                .Property(c => c.DataInicio);
            modelBuilder.Entity<Contrato>()
                .Property(c => c.Numero);
            modelBuilder.Entity<Contrato>()
                .HasOne(c => c.Cliente)
                .WithMany()
                .HasForeignKey(c => c.ClienteId)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<Contrato>()
                .HasOne(c => c.Plano)
                .WithMany()
                .HasForeignKey(c => c.PlanoId)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<Contrato>()
                .HasOne(c => c.Corretor)
                .WithMany()
                .HasForeignKey(c => c.CorretorId)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<Contrato>()
                .HasOne(c => c.Corretora)
                .WithMany()
                .HasForeignKey(c => c.CorretoraId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
