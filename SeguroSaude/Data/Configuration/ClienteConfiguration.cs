﻿using Microsoft.EntityFrameworkCore;
using SeguroSaude.Entity;

namespace SeguroSaude.Data.Configuration
{
    public static class ClienteConfiguration
    {
        public static void Configure(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cliente>()
                .HasKey(c => c.Id);
            modelBuilder.Entity<Cliente>()
                .Property(c => c.Ativo);
            modelBuilder.Entity<Cliente>()
                .Property(c => c.ClienteDesde);
            modelBuilder.Entity<Cliente>()
                .Property(c => c.CPF)
                .HasMaxLength(11);
            modelBuilder.Entity<Cliente>()
                .Property(c => c.DataNascimento);
            modelBuilder.Entity<Cliente>()
                .Property(c => c.Email)
                .HasMaxLength(200);
            modelBuilder.Entity<Cliente>()
                .Property(c => c.Endereco)
                .HasMaxLength(400);
            modelBuilder.Entity<Cliente>()
                .Property(c => c.Nome)
                .HasMaxLength(150);
            modelBuilder.Entity<Cliente>()
                .Property(c => c.Telefone)
                .HasMaxLength(11);
        }
    }
}
