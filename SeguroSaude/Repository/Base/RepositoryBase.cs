﻿using SeguroSaude.Data;
using System;
using System.Linq;

namespace SeguroSaude.Repository.Base
{
    public class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        private ApplicationDbContext _dbContext;
        public RepositoryBase(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public void Create(T entity)
        {
            _dbContext.Add(entity);
            _dbContext.SaveChanges();
        }

        public void Delete(Guid id)
        {
            var entity = Read(id);
            _dbContext.Remove(entity);
            _dbContext.SaveChanges();
        }

        public T Read(Guid id)
        {
            return _dbContext.Find<T>(id);
        }

        public IQueryable<T> ReadAll()
        {
            return _dbContext.Set<T>();
        }

        public void Update(T entity)
        {
            _dbContext.Update(entity);
            _dbContext.SaveChanges();
        }
    }
}
