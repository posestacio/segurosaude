﻿using System;
using System.Linq;

namespace SeguroSaude.Repository.Base
{
    public interface IRepositoryBase<T> where T : class
    {
        void Create(T entity);
        void Update(T entity);
        void Delete(Guid id);
        IQueryable<T> ReadAll();
        T Read(Guid id);
    }
}
