﻿using Microsoft.AspNetCore.Http;
using System;
using System.Linq;

namespace SeguroSaude.Models.Helpers
{
    public static class ModelHelper<T> where T : class
    {
        public static T GetFormData(IFormCollection form)
        {
            var model = (T)Activator.CreateInstance<T>();

            foreach (var property in typeof(T).GetProperties())
            {
                var propertyName = property.Name.ToLower();
                if (form.Keys.Any(x => x == propertyName))
                {
                    if (property.PropertyType == typeof(String))
                    {
                        property.SetValue(model, form[propertyName].FirstOrDefault());
                    }
                    if (property.PropertyType == typeof(System.Guid))
                    {
                        property.SetValue(model, Guid.Parse(form[propertyName].FirstOrDefault()));
                    }
                    if (property.PropertyType == typeof(int))
                    {
                        property.SetValue(model, Convert.ToInt32(form[propertyName].FirstOrDefault()));
                    }
                    if (property.PropertyType == typeof(DateTime))
                    {
                        property.SetValue(model, Convert.ToDateTime(form[propertyName].FirstOrDefault()));
                    }
                }
            }

            return model;
        }
    }
}
