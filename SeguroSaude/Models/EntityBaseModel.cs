﻿using System;

namespace SeguroSaude.Models
{
    public class EntityBaseModel
    {
        public Guid Id { get; set; }
        public bool Ativo { get; set; }
    }
}
