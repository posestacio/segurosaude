﻿namespace SeguroSaude.Models
{
    public class OperadoraModel : PessoaJuridicaModel
    {
        public string Logo { get; set; }
    }
}
