﻿namespace SeguroSaude.Models
{
    public class PessoaJuridicaModel : PessoaModel
    {
        public string CNPJ { get; set; }
        public string RazaoSocial { get; set; }
    }
}
